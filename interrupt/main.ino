
#define INT_PIN 2

volatile int state = 0;

void setup(){
  Serial.begin(9600);
  pinMode(INT_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(INT_PIN), isr, CHANGE);
}

void loop() {
  Serial.println(state);
  delay(1000);
}

void isr() {
  Serial.println("HEY"); // Printing doesn't work on interrupts because Serial also works with an interrupt
  if (digitalRead(INT_PIN) == HIGH)
    state = 1;
  else
    state = 2;
}
