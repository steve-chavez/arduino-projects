#include "SPI.h"
#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"

#define TFT_DC 9
#define TFT_CS 10

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

#define MAX_COUNTER 32767 //16 bit int, -32,768 to 32,767 range
int counter = 0; 

void setup() {
  tft.begin();
  tft.setRotation(3); //Landscape
  tft.fillScreen(ILI9341_BLACK); //Background
  tft.setCursor(0, 0); //Top left
  tft.setTextColor(ILI9341_WHITE); tft.setTextSize(2);
  tft.print("The counter is:");
  //Text size of what's to come
  tft.setTextSize(5);//A size of 1 is 6x8 per char, size of 5 then is 30x40
}

void loop(void) {
  tft.setTextColor(ILI9341_GREEN, ILI9341_BLACK);//We set the color again with black background to clear previous text
  if(counter == MAX_COUNTER){
    counter = 0;
    tft.fillRect(0, 100, 320, 40, ILI9341_BLACK);//Clear whole width with text height
  }
  // Center cursor
  tft.setCursor(160 - 30*num_length(counter)/2, 100);//160x120 Center of the screen, 100 = 120 - height_of_char/2
  tft.println(counter++);
  delay(1000);
}

// From https://stackoverflow.com/questions/3068397/finding-the-length-of-an-integer-in-c
int num_length(int n){
  return n == 0 ? 1 : floor(log10(n)) + 1; // log10 of 0 is undefined
}
