# Arduino projects

## To verify and upload from command line

```bash
  arduino --verify file.ino
  arduino --upload file.ino
```

The `arduino` command picks the libraries from the libraries dir, this is dependent on sketchbook.path on `.arduino15/preferences.txt`,
it must point to this folder.

## To use serial monitor in cli

```bash
  tail -f /dev/ttyUSB0
```

The user needs to be in the `dialout` linux group.
