#include <dht.h>

dht DHT;

#define DHT11_PIN 7

void setup(){
  Serial.begin(9600);
}

void loop(){
  int chk = DHT.read11(DHT11_PIN); //read11 because we're using dht11
  Serial.print("Check:");
  Serial.println(chk); // This gives -2 if a timeout occurred, communication failed, -1 is a checksum error, 0 is ok
  Serial.print("Temperature = ");
  Serial.print(DHT.temperature);
  Serial.println("°C");
  Serial.print("Humidity = ");
  Serial.print(DHT.humidity);
  Serial.println("%");
  delay(3000); //When this is at 1000 intervals it gives -999 readings, corresponding to chk -2
}
