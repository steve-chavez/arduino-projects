#include <Adafruit_GFX_AS.h>
#include <Adafruit_ILI9341_AS.h>
#include <dht.h>
#include <SPI.h>

//------- Ring meter lib defines, ignore ------
#define sclk 13
#define mosi 11
#define cs   10
#define dc   9
#define rst  7
#define RED2RED 0
#define GREEN2GREEN 1
#define BLUE2BLUE 2
#define BLUE2RED 3
#define GREEN2RED 4
#define RED2GREEN 5
#define ILI9341_GREY 0x2104
//---------------------------------------------


#define DHT11_PIN 7
#define MQ135_PIN A0

#define INT_PIN 2

#define MIN_TEMP 0
#define MAX_TEMP 35  // 33 is Lima highest recorded temperature

#define MIN_HUM  0
#define MAX_HUM  100

#define MIN_PPM  0
#define MAX_PPM  2000 // 2000 is the amount of CO2 that starts to cause tiredness, headaches

#define MAX_SAVES 9

enum motion {
  UP,
  DOWN,
  NONE
};

enum lcdState {
  SHOWING_CO2_BAR_PLOT,
  SHOWING_HUMIDITY_BAR_PLOT,
  SHOWING_TEMPERATURE_BAR_PLOT,
  SHOWING_RING_METERS,
  SAVING
};

dht DHT;
Adafruit_ILI9341_AS tft = Adafruit_ILI9341_AS(cs, dc, rst); // Invoke custom library

void setup(void) {
  pinMode(INT_PIN, INPUT);
  attachInterrupt(digitalPinToInterrupt(INT_PIN), interruptHandler, CHANGE);

  tft.init();
  tft.setRotation(3);
  tft.fillScreen(ILI9341_BLACK);
  tft.setTextSize(1);
}

volatile motion globalMotion = NONE; //volatile because we use it with interrupts
lcdState globalLcdState = SHOWING_RING_METERS;

int savedPpms[MAX_SAVES]         = {0, 0, 0, 0, 0, 0, 0, 0, 0};
int savedHumidities[MAX_SAVES]   = {0, 0, 0, 0, 0, 0, 0, 0, 0};
int savedTemperatures[MAX_SAVES] = {0, 0, 0, 0, 0, 0, 0, 0, 0};
int numberOfSaves = 0;

void loop() {
  delay(2000);
  int chk = DHT.read11(DHT11_PIN);
  int temperature = DHT.temperature;
  int humidity = DHT.humidity;
  int ppm = analogRead(MQ135_PIN);

  //If we just saved then redirect to main screen
  if(globalLcdState == SAVING){
    tft.fillScreen(ILI9341_BLACK);
    globalLcdState = SHOWING_RING_METERS;
  }

  lcdState currentState = newState(globalLcdState, globalMotion);

  //Clear screen if state has changed
  if(currentState != globalLcdState)
    tft.fillScreen(ILI9341_BLACK);

  //Update global state
  globalLcdState = currentState;

  switch(globalLcdState){
    case SHOWING_CO2_BAR_PLOT:{
      // Draw cartesian plane
      tft.drawLine(32, 10,  32,  220, ILI9341_ORANGE); //X axis
      tft.drawLine(32, 220, 310, 220, ILI9341_ORANGE); //Y axis

      // Draw reference ppms
      tft.setTextColor(ILI9341_CYAN, ILI9341_BLACK);
      tft.drawNumber(MAX_PPM,     0, 10,  2); tft.drawLine(30, 10,  34, 10,  ILI9341_ORANGE);
      tft.drawNumber(4*MAX_PPM/5, 0, 52,  2); tft.drawLine(30, 52,  34, 52,  ILI9341_ORANGE);
      tft.drawNumber(3*MAX_PPM/5, 0, 94,  2); tft.drawLine(30, 94,  34, 94,  ILI9341_ORANGE);
      tft.drawNumber(2*MAX_PPM/5, 7, 136, 2); tft.drawLine(30, 136, 34, 136, ILI9341_ORANGE);
      tft.drawNumber(1*MAX_PPM/5, 7, 178, 2); tft.drawLine(30, 178, 34, 178, ILI9341_ORANGE);
      tft.drawNumber(MIN_PPM,    20, 220, 2); tft.drawLine(30, 220, 34, 220, ILI9341_ORANGE);

      tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);
      // Draw saved ppms
      for(int i = 0; i < numberOfSaves; i++){
        float ratio = savedPpms[i]/(float)MAX_PPM; // ENSURE THE (float) is present otherwise this doesn't work !!!!!
        int rectHeight = 210 * ratio; //Getting a rectangle size proportional to the ppm
        tft.fillRect(50 + 30*i, 220 - rectHeight, 20, rectHeight, ILI9341_GREEN);
        tft.drawNumber(i + 1, 58 + 30*i, 222, 2);
      }
      break;
    }
    case SHOWING_HUMIDITY_BAR_PLOT:{
      // Draw cartesian plane
      tft.drawLine(25, 10,  25,  220, ILI9341_ORANGE); //X axis
      tft.drawLine(25, 220, 310, 220, ILI9341_ORANGE); //Y axis

      // Draw reference humidities
      tft.setTextColor(ILI9341_CYAN, ILI9341_BLACK);
      tft.drawNumber(MAX_HUM,     0, 10,  2); tft.drawLine(23, 10,  27, 10,  ILI9341_ORANGE);
      tft.drawNumber(4*MAX_HUM/5, 5, 52,  2); tft.drawLine(23, 52,  27, 52,  ILI9341_ORANGE);
      tft.drawNumber(3*MAX_HUM/5, 5, 94,  2); tft.drawLine(23, 94,  27, 94,  ILI9341_ORANGE);
      tft.drawNumber(2*MAX_HUM/5, 5, 136, 2); tft.drawLine(23, 136, 27, 136, ILI9341_ORANGE);
      tft.drawNumber(1*MAX_HUM/5, 5, 178, 2); tft.drawLine(23, 178, 27, 178, ILI9341_ORANGE);
      tft.drawNumber(MIN_HUM,    13, 220, 2); tft.drawLine(23, 220, 27, 220, ILI9341_ORANGE);

      // Draw saved humidities
      tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);
      for(int i = 0; i < numberOfSaves; i++){
        int rectHeight = 210 * savedHumidities[i]/MAX_HUM; //Getting a rectangle size proportional to the humidity
        tft.fillRect(45 + 30*i, 220 - rectHeight, 20, rectHeight, ILI9341_BLUE);
        tft.drawNumber(i + 1, 51 + 30*i, 222, 2);
      }
      break;
    }
    case SHOWING_TEMPERATURE_BAR_PLOT:{
      // Draw cartesian plane
      tft.drawLine(20, 10,  20,  220, ILI9341_ORANGE); //X axis
      tft.drawLine(20, 220, 310, 220, ILI9341_ORANGE); //Y axis

      // Draw reference temperatures
      tft.setTextColor(ILI9341_CYAN, ILI9341_BLACK);
      tft.drawNumber(MAX_TEMP,     0, 10,  2); tft.drawLine(18, 10,  22, 10,  ILI9341_ORANGE);
      tft.drawNumber(4*MAX_TEMP/5, 0, 52,  2); tft.drawLine(18, 52,  22, 52,  ILI9341_ORANGE);
      tft.drawNumber(3*MAX_TEMP/5, 0, 94,  2); tft.drawLine(18, 94,  22, 94,  ILI9341_ORANGE);
      tft.drawNumber(2*MAX_TEMP/5, 0, 136, 2); tft.drawLine(18, 136, 22, 136, ILI9341_ORANGE);
      tft.drawNumber(1*MAX_TEMP/5, 8, 178, 2); tft.drawLine(18, 178, 22, 178, ILI9341_ORANGE);
      tft.drawNumber(MIN_TEMP,     8, 220, 2); tft.drawLine(18, 220, 22, 220, ILI9341_ORANGE);

      // Draw saved temperatures
      tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);
      for(int i = 0; i < numberOfSaves; i++){
        int rectHeight = 210 * savedTemperatures[i]/MAX_TEMP; //Getting a rectangle size proportional to the temperature
        tft.fillRect(40 + 30*i, 220 - rectHeight, 20, rectHeight, ILI9341_RED);
        tft.drawNumber(i + 1, 46 + 30*i, 222, 2);
      }
      break;
    }
    case SHOWING_RING_METERS:{
      int xpos = 0, ypos = 40, gap = 4, radius = 52;
      xpos = gap + ringMeter(temperature, MIN_TEMP, MAX_TEMP, xpos, ypos, radius, "C", BLUE2RED);
      xpos = gap + ringMeter(humidity, MIN_HUM, MAX_HUM, xpos, ypos, radius, "%", BLUE2RED);
      ringMeter(ppm, MIN_PPM, MAX_PPM, xpos, ypos, radius, "ppm", BLUE2RED);
      tft.setTextColor(ILI9341_RED, ILI9341_BLACK);
      tft.drawString("Temp", 20, 160, 4);
      tft.setTextColor(ILI9341_BLUE, ILI9341_BLACK);
      tft.drawString("Hum", 135, 160, 4);
      tft.setTextColor(ILI9341_GREEN, ILI9341_BLACK);
      tft.drawString("CO2", 240, 160, 4);
      break;
    }
    case SAVING:{
      numberOfSaves++;
      if(numberOfSaves > MAX_SAVES) numberOfSaves = 1;
      tft.setTextColor(ILI9341_CYAN, ILI9341_BLACK);
      tft.drawString("Guardando lecturas en:", 20, 100, 4);
      tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);
      tft.drawNumber(numberOfSaves, 152, 130, 4);
      savedTemperatures[numberOfSaves - 1] = temperature;
      savedHumidities[numberOfSaves - 1] = humidity;
      savedPpms[numberOfSaves - 1] = ppm;
      break;
    }
  }

  globalMotion = NONE;
}

void interruptHandler(){
  if (digitalRead(INT_PIN) == HIGH)
    globalMotion = UP;
  else
    globalMotion = DOWN;
}

lcdState newState(lcdState previousState, motion newMotion){
  if(previousState == SHOWING_CO2_BAR_PLOT && newMotion == DOWN){
    return SHOWING_HUMIDITY_BAR_PLOT;
  } else
  if(previousState == SHOWING_HUMIDITY_BAR_PLOT && newMotion == UP){
    return SHOWING_CO2_BAR_PLOT;
  } else
  if(previousState == SHOWING_HUMIDITY_BAR_PLOT && newMotion == DOWN){
    return SHOWING_TEMPERATURE_BAR_PLOT;
  } else
  if(previousState == SHOWING_TEMPERATURE_BAR_PLOT && newMotion == UP){
    return SHOWING_HUMIDITY_BAR_PLOT;
  } else
  if(previousState == SHOWING_TEMPERATURE_BAR_PLOT && newMotion == DOWN){
    return SHOWING_RING_METERS;
  } else
  if(previousState == SHOWING_RING_METERS && newMotion == UP){
    return SHOWING_TEMPERATURE_BAR_PLOT;
  } else
  if(previousState == SHOWING_RING_METERS && newMotion == DOWN){
    return SAVING;
  } else
  if(previousState == SAVING && newMotion == UP){
    return SHOWING_RING_METERS;
  } else
    return previousState;
}

// -------------------------------------------------------------------------------
// -------------------------Ring meter lib, just ignore it------------------------
// -------------------------------------------------------------------------------
int ringMeter(int value, int vmin, int vmax, int x, int y, int r, char *units, byte scheme) {
  x += r; y += r;
  int w = r / 4;
  int angle = 150;
  int text_colour = 0;
  int v = map(value, vmin, vmax, -angle, angle);
  byte seg = 5;
  byte inc = 5;
  for (int i = -angle; i < angle; i += inc) {
    int colour = 0;
    switch (scheme) {
      case 0: colour = ILI9341_RED; break;
      case 1: colour = ILI9341_GREEN; break;
      case 2: colour = ILI9341_BLUE; break;
      case 3: colour = rainbow(map(i, -angle, angle, 0, 127)); break;
      case 4: colour = rainbow(map(i, -angle, angle, 63, 127)); break;
      case 5: colour = rainbow(map(i, -angle, angle, 127, 63)); break;
      default: colour = ILI9341_BLUE; break;
    }

    float sx = cos((i - 90) * 0.0174532925);
    float sy = sin((i - 90) * 0.0174532925);
    uint16_t x0 = sx * (r - w) + x;
    uint16_t y0 = sy * (r - w) + y;
    uint16_t x1 = sx * r + x;
    uint16_t y1 = sy * r + y;

    float sx2 = cos((i + seg - 90) * 0.0174532925);
    float sy2 = sin((i + seg - 90) * 0.0174532925);
    int x2 = sx2 * (r - w) + x;
    int y2 = sy2 * (r - w) + y;
    int x3 = sx2 * r + x;
    int y3 = sy2 * r + y;

    if (i < v) {
      tft.fillTriangle(x0, y0, x1, y1, x2, y2, colour);
      tft.fillTriangle(x1, y1, x2, y2, x3, y3, colour);
      text_colour = colour;
    }
    else
    {
      tft.fillTriangle(x0, y0, x1, y1, x2, y2, ILI9341_GREY);
      tft.fillTriangle(x1, y1, x2, y2, x3, y3, ILI9341_GREY);
    }
  }

  char buf[10];
  byte len = 4; if (value > 999) len = 5;
  dtostrf(value, len, 0, buf);

  tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);

  if (r > 84) tft.drawCentreString(buf, x - 5, y - 20, 6);
  else tft.drawCentreString(buf, x - 5, y - 20, 4);

  tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);
  if (r > 84) tft.drawCentreString(units, x, y + 30, 4);
  else tft.drawCentreString(units, x, y + 5, 2);

  return x + r;
}

// -------------------------------------------------------------------------------
// -------------------------Ring meter lib, just ignore it------------------------
// -------------------------------------------------------------------------------
unsigned int rainbow(byte value) {
  byte red = 0;
  byte green = 0;
  byte blue = 0;

  byte quadrant = value / 32;

  if (quadrant == 0) {
    blue = 31;
    green = 2 * (value % 32);
    red = 0;
  }
  if (quadrant == 1) {
    blue = 31 - (value % 32);
    green = 63;
    red = 0;
  }
  if (quadrant == 2) {
    blue = 0;
    green = 63;
    red = value % 32;
  }
  if (quadrant == 3) {
    blue = 0;
    green = 63 - 2 * (value % 32);
    red = 31;
  }
  return (red << 11) + (green << 5) + blue;
}
