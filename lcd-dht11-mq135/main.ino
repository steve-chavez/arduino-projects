#include "Adafruit_GFX.h"
#include "Adafruit_ILI9341.h"
#include <dht.h>
#include "SPI.h"

#define DHT11_PIN 7
#define MQ135_PIN A0
#define TFT_DC 9
#define TFT_CS 10

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);

dht DHT;

void setup() {
  tft.begin();
  tft.setRotation(3); //Landscape
  tft.fillScreen(ILI9341_BLACK); //Background
  tft.setCursor(0, 0); //Top left
  //Text size of what's to come
  tft.setTextSize(2);
}

void loop(void) {
  int chk = DHT.read11(DHT11_PIN); //read11 because we're using dht11
  int co2_ppm = analogRead(MQ135_PIN);
  tft.setTextColor(ILI9341_RED, ILI9341_BLACK);
  tft.setCursor(40, 50);
  tft.print("Temperatura = ");
  tft.print((int)DHT.temperature);
  tft.println(" C");
  tft.setCursor(40, 100);
  tft.setTextColor(ILI9341_BLUE, ILI9341_BLACK);
  tft.print("Humedad = ");
  tft.print((int)DHT.humidity);
  tft.println("%");
  tft.setCursor(40, 150);
  tft.setTextColor(ILI9341_GREEN, ILI9341_BLACK);
  tft.print("CO2 = ");
  tft.print((int)co2_ppm);
  tft.println(" ppm");
  delay(2000);
}