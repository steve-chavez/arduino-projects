#define NUM_PINS 3

int pins[NUM_PINS] = {4, 5, 6};

void setup() {
  pinMode(pins[0], OUTPUT);
  pinMode(pins[1], OUTPUT);
  pinMode(pins[2], OUTPUT);
}

void loop() {
  for(int i = 0; i <= NUM_PINS; i++)
    blink(pins[i]);
}

void blink(const int pin){
  digitalWrite(pin, HIGH);
  delay(1000);
  digitalWrite(pin, LOW);
  delay(1000);
}
